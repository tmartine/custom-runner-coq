#!/builds/orchestrator/bin/python

# Standard library
import logging
import os
import subprocess

# Third-party imports
import cs
import zmq

# Project module
import base


def main(logger: logging.LoggerAdapter) -> None:
    job_info = base.get_job_info()
    if isinstance(job_info.image.contents, base.Test):
        test(job_info.image.contents)
        return
    cloudstack = base.Config.load().get_cloudstack()
    vm = base.find_vm_by_job(cloudstack, job_info)
    vm_name = vm["name"]
    logger.info(f"Clean up VM {vm_name}, image {job_info.image.contents}...")
    if base.has_tag(vm, base.RUNNER_TAG_KEY):
        logger.info(f"Order to destroy VM {vm_name}...")
        socket = open_socket()
        order = base.DestroyOrder(job_info=job_info, vm_name=vm_name)
        socket.send_string(order.model_dump_json())
        socket.recv()
    else:
        logger.info(f"Recycle unused VM {vm_name}...")
        base.recycle_vm(logger, cloudstack, vm, job_info)


def test(info: base.Test):
    pass


def open_socket() -> zmq.Socket:
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(base.get_destroy_socket_uri())
    return socket


if __name__ == "__main__":
    base.execute_with_loggers("cleanup", main)
