#!/builds/orchestrator/bin/python

# Standard library
import io
import json
import logging
import os
import shlex
import sys
import typing

# Third-party imports
import fabric
import invoke

# Project module
import base


def main(logger: logging.LoggerAdapter) -> None:
    job_info = base.get_job_info()
    if isinstance(job_info.image.contents, base.Test):
        test(job_info.image.contents)
        return
    vm = base.find_vm_by_job(base.Config.load().get_cloudstack(), job_info)
    vm_ip = base.get_vm_ip_address(vm)
    connection = fabric.Connection(f"ci@{vm_ip}", connect_kwargs={"password": "ci"})
    connect(logger, connection, job_info.image.contents.options.docker)


def test(info: base.Test):
    pass


def connect(
    logger: logging.LoggerAdapter,
    connection: fabric.Connection,
    docker: typing.Optional[str],
):
    command_file = sys.argv[1]
    step_name = sys.argv[2]
    if step_name not in [
        "before_script",
        "build_script",
        "step_script",
        "script",
        "after_script",
    ]:
        docker = None
    if docker is None:
        run_command = f"""
            if echo ci | sudo -l -S -p "" | grep -q "NOPASSWD: ALL"; then
                sudo bash
            elif echo ci | sudo -l -S -p "" | grep -q ALL; then
                (echo ci; cat) | sudo -k -S -p "" bash
            else
                bash
            fi
        """
    else:
        entrypoint_str = os.environ.get("entrypoint")
        if entrypoint_str is None:
            entrypoint = []
        else:
            entrypoint = json.loads(entrypoint_str)
        ci_registry = os.environ.get("CUSTOM_ENV_CI_REGISTRY")
        if ci_registry is not None:
            user = os.environ["CUSTOM_ENV_CI_REGISTRY_USER"]
            password = os.environ["CUSTOM_ENV_CI_REGISTRY_PASSWORD"]
            password_stream = io.StringIO(password)
            connection.run(
                f"docker login --username {user} --password-stdin {ci_registry}",
                in_stream=password_stream,
            )
        run_list = ["docker", "run", "-i"]
        run_list.extend(["-v", "/builds:/builds"])
        run_list.extend(["-v", "/tmp:/tmp"])
        run_list.extend(["-v", "/var/run/docker.sock:/var/run/docker.sock"])
        if entrypoint:
            run_list.extend(["--entrypoint", entrypoint[0]])
        run_list.append(docker)
        run_list.extend(entrypoint[1:])
        run_list.extend(
            [
                "sh",
                "-c",
                """\
if [ -x /usr/local/bin/bash ]; then
	exec /usr/local/bin/bash
elif [ -x /usr/bin/bash ]; then
	exec /usr/bin/bash
elif [ -x /bin/bash ]; then
	exec /bin/bash
elif [ -x /usr/local/bin/sh ]; then
	exec /usr/local/bin/sh
elif [ -x /usr/bin/sh ]; then
	exec /usr/bin/sh
elif [ -x /bin/sh ]; then
	exec /bin/sh
elif [ -x /busybox/sh ]; then
	exec /busybox/sh
else
	echo shell not found
	exit 1
fi

""",
            ]
        )
        run_command = shlex.join(run_list)
    base.make_connection_in_stream_fast(connection)
    with open(command_file) as in_stream:
        try:
            connection.run(run_command, in_stream=in_stream)
        except invoke.exceptions.UnexpectedExit as e:
            exit_code = e.result.exited
            logger.exception(f"Exit code {exit_code}")
            sys.exit(exit_code)


if __name__ == "__main__":
    base.execute_with_loggers("run", main)
